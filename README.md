# scb-book-store-service

## Create and run database for BookStore Service

### use docker, docker-compose [get started docker](https://www.docker.com/get-started)

- ```cd docker/```
- ```docker-compose up -d```

### view logs

- ```docker-compose logs -f```

### stop database

- ```docker-compose down```

## Postman ([Postman](https://www.getpostman.com/downloads/))

- Import API file ```SCB.postman_collection.json```
    - Order priority by API name ```example '(1) [POST] users' or (2) [POST] Login ```

- Import ENV file ```Bookstore.postman_environment.json```

## Register user

> url http://localhost:2019/users

- Method: POST
- Body (form-data)
    - ```username: "john.doe"```
    - ```password: "mypassword"```
    - ```date_of_birth: "15/01/1985"```

## Login app

> url http://localhost:2019/oauth/token

- Method: POST
- Authorization
    - ```Type: Basic Auth```
    - ```Username: clientId```
    - ```Password: clientSecret```
- Body (form-data)
    - ```grant_type: password```
    - ```username: john.doe```
    - ```password: mypassword```
- Response
    - access_token
    - token_type

## Other API

> Set Header request

- ```Authorization: {response.token_type} {response.access_token}```

> Example

- ```Authorization: bearer eyjndkJkfjsk8djfhssd...```

## Scheduled Task

> Scheduled is running when startup application and every 00:05 on Sunday for update list book publisher

## [URL - LOCAL]

- AppInfo = http://localhost:2019/appinfo
- Swagger_API = http://localhost:2019/swagger-ui.html
- Actuator_Status = http://localhost:2019/actuator/health
