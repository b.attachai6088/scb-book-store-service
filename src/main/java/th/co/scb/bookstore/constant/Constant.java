package th.co.scb.bookstore.constant;

public class Constant {
    public interface UrlPublisher {
        String BOOK = "https://scb-test-book-publisher.herokuapp.com/books";
        String BOOK_RECOMMENDATION = BOOK + "/recommendation";
    }
}
