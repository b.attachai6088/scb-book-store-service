package th.co.scb.bookstore.model;

import lombok.Data;

@Data
public class BookPublisherModelResponse {
    private Integer id;
    private String book_name;
    private String author_name;
    private Double price;
}
