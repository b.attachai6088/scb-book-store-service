package th.co.scb.bookstore.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class OrderModelRequest {
    @NotEmpty
    private Integer[] orders;
}
