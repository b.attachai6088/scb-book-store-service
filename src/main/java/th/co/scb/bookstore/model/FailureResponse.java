package th.co.scb.bookstore.model;

import java.util.Date;

public class FailureResponse<T> {
    private T errors;
    private int status;

    public FailureResponse() {
    }

    public FailureResponse(T errors, int status) {
        this.errors = errors;
        this.status = status;
    }

    public T getErrors() {
        return errors;
    }

    public void setErrors(T errors) {
        this.errors = errors;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getTimestamp() {
        return new Date();
    }
}
