package th.co.scb.bookstore.model;

import lombok.Data;

@Data
public class BookModelResponse {
    private Integer id;
    private String name;
    private String author;
    private Double price;
    private Boolean is_recommended;
}
