package th.co.scb.bookstore.model;

import lombok.Data;

@Data
public class UserModelResponse {
    private String name;
    private String surname;
    private String date_of_birth;
    private Integer[] books;
}
