package th.co.scb.bookstore.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class LoginModelRequest {
    @NotEmpty
    @Size(min = 1, max = 20)
    private String username;

    @NotEmpty
    @Size(min = 1, max = 20)
    private String password;
}
