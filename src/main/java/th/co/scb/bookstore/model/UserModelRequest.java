package th.co.scb.bookstore.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class UserModelRequest {
    @NotEmpty
    @Size(min = 1, max = 20)
    private String username;

    @NotEmpty
    @Size(min = 1, max = 20)
    private String password;

    @NotEmpty
    @Size(min = 1, max = 10)
    private String date_of_birth;
}
