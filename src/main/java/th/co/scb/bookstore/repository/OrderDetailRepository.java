package th.co.scb.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import th.co.scb.bookstore.entity.OrderDetail;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Integer> {
}
