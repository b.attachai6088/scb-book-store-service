package th.co.scb.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import th.co.scb.bookstore.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsername(String username);
}
