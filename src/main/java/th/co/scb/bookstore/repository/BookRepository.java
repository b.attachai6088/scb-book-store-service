package th.co.scb.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import th.co.scb.bookstore.entity.Book;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Integer> {
    List<Book> findAllByOrderByIsRecommendedDesc();

    @Query(value = "select od.book_id from bookstoredb.orders o join bookstoredb.order_detail od on o.id = od.order_id where o.user_id = ?1 group by od.book_id", nativeQuery = true)
    Integer[] findListBookByUserId(Integer userId);
}
