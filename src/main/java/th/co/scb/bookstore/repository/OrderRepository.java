package th.co.scb.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import th.co.scb.bookstore.entity.Orders;

import java.util.List;

public interface OrderRepository extends JpaRepository<Orders, Integer> {
    List<Orders> findAllByUserId(Integer userId);
}
