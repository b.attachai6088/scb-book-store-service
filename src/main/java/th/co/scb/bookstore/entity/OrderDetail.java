package th.co.scb.bookstore.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "order_detail", schema = "bookstoredb")
public class OrderDetail implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, length = 10)
    private Integer id;

    @Column(name = "order_id", nullable = false, length = 10)
    private Integer orderId;

    @Column(name = "book_id", nullable = false, length = 10)
    private Integer bookId;

    @ManyToOne
    @JoinColumn(name = "order_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    private Orders ordersByOrderId;
}
