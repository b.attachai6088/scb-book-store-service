package th.co.scb.bookstore.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "book", schema = "bookstoredb")
public class Book implements Serializable {
    @Id
    @Column(name = "id", nullable = false, length = 10)
    private Integer id;

    @Column(name = "name", nullable = false, length = 200)
    private String name;

    @Column(name = "author", nullable = false, length = 200)
    private String author;

    @Column(name = "price", nullable = false)
    private Double price;

    @Column(name = "is_recommended", nullable = false, length = 1)
    private Boolean isRecommended;

    @Column(name = "create_date", nullable = false)
    private Date createDate;
}
