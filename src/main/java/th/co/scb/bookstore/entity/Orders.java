package th.co.scb.bookstore.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "orders", schema = "bookstoredb")
public class Orders implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, length = 10)
    private Integer id;

    @Column(name = "user_id", nullable = false, length = 10)
    private Integer userId;

    @Column(name = "sum_price", nullable = false)
    private Double sumPrice;

    @Column(name = "create_date", nullable = false)
    private Date createDate;

    @OneToMany(mappedBy = "ordersByOrderId")
    List<OrderDetail> orderDetailsById;
}
