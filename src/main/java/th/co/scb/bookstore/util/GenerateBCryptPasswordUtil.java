package th.co.scb.bookstore.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class GenerateBCryptPasswordUtil {
    private static final String AHEAD_B_CRYPT = "{bcrypt}";

    public static String generateJdbcBCryptPassword(String password) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return String.format("%1$s%2$s", AHEAD_B_CRYPT, bCryptPasswordEncoder.encode(password));
    }
}
