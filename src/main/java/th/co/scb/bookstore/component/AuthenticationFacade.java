package th.co.scb.bookstore.component;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import th.co.scb.bookstore.exception.InvalidTokenException;

import java.io.Serializable;
import java.util.Map;

@Component
public class AuthenticationFacade implements Serializable {

    @Autowired
    private TokenStore tokenStore;

    private Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public DataUserLogin getUserLogin() {
        DataUserLogin user = null;
        Authentication authentication = getAuthentication();
        if (!ObjectUtils.isEmpty(authentication.getDetails())
                && (authentication.getDetails() instanceof OAuth2AuthenticationDetails)) {
            OAuth2AuthenticationDetails oAuth2AuthenticationDetails = (OAuth2AuthenticationDetails) authentication
                    .getDetails();
            String jwtToken = oAuth2AuthenticationDetails.getTokenValue();
            if (!ObjectUtils.isEmpty(jwtToken)) {
                Map<String, Object> details = tokenStore.readAccessToken(oAuth2AuthenticationDetails.getTokenValue()).getAdditionalInformation();
                return new DataUserLogin(details);
            } else {
                throw new InvalidTokenException("invalid token");
            }
        } else {
            throw new InvalidTokenException("invalid token");
        }
    }

    @Getter
    public class DataUserLogin {
        private Integer userId;
        private String username;

        DataUserLogin(Map<String, Object> details) {
            this.userId = (Integer) details.getOrDefault("user_id", 0);
            this.username = (String) details.getOrDefault("user_name", "");
        }
    }
}
