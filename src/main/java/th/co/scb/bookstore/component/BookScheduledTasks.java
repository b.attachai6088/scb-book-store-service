package th.co.scb.bookstore.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import th.co.scb.bookstore.service.BookService;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class BookScheduledTasks {

    private static final Logger logger = LoggerFactory.getLogger(BookScheduledTasks.class);
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    private BookService bookService;

    @Scheduled(cron = "* 5 0 * * SUN") // At 00:05 on Sunday
    public void getBookPublisher() {
        logger.info("BookScheduledTasks running at : " + dateFormat.format(new Date()));
        this.bookService.getBookPublisher();
    }
}
