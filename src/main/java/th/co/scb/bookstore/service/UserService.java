package th.co.scb.bookstore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import th.co.scb.bookstore.entity.Book;
import th.co.scb.bookstore.entity.OrderDetail;
import th.co.scb.bookstore.entity.Orders;
import th.co.scb.bookstore.entity.User;
import th.co.scb.bookstore.exception.OrderErrorException;
import th.co.scb.bookstore.exception.UserNotFoundException;
import th.co.scb.bookstore.model.OrderModelRequest;
import th.co.scb.bookstore.model.UserModelRequest;
import th.co.scb.bookstore.model.UserModelResponse;
import th.co.scb.bookstore.repository.BookRepository;
import th.co.scb.bookstore.repository.OrderDetailRepository;
import th.co.scb.bookstore.repository.OrderRepository;
import th.co.scb.bookstore.repository.UserRepository;
import th.co.scb.bookstore.util.GenerateBCryptPasswordUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    public UserModelResponse getUserDetail(Integer userId) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        User user = findUser(userId);
        UserModelResponse userModelResponse = new UserModelResponse();
        userModelResponse.setName(user.getName());
        userModelResponse.setSurname(user.getSurname());
        userModelResponse.setDate_of_birth(dateFormat.format(user.getDateOfBirth()));
        userModelResponse.setBooks(bookRepository.findListBookByUserId(userId));
        return userModelResponse;
    }

    public void deleteUser(Integer userId) {
        User user = findUser(userId);
        List<Orders> ordersList = orderRepository.findAllByUserId(user.getId());
        for (Orders orders : ordersList) {
            orderDetailRepository.deleteAll(orders.getOrderDetailsById());
        }
        orderRepository.deleteAll(ordersList);
        userRepository.deleteById(user.getId());
    }

    public void saveUser(UserModelRequest userModelRequest) {
        User user = userRepository.findByUsername(userModelRequest.getUsername());
        if (user != null) {
            throw new DuplicateKeyException("Duplicate user");
        }

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            User userModel = new User();
            userModel.setUsername(userModelRequest.getUsername());
            userModel.setPassword(GenerateBCryptPasswordUtil.generateJdbcBCryptPassword(userModelRequest.getPassword()));
            userModel.setDateOfBirth(dateFormat.parse(userModelRequest.getDate_of_birth()));
            userModel.setName(userModelRequest.getUsername().split("\\.")[0]);
            userModel.setSurname(userModelRequest.getUsername().split("\\.")[1]);
            userModel.setCreateDate(new Date());
            userRepository.save(userModel);
        } catch (ParseException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Invalid format date");
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Invalid username");
        }
    }

    public Double getPrice(OrderModelRequest orderModelRequest, Integer userId) {
        User user = findUser(userId);
        List<Book> list = bookRepository.findAll().stream().filter(b -> Arrays.asList(orderModelRequest.getOrders()).contains(b.getId())).collect(Collectors.toList());
        if (list.size() != orderModelRequest.getOrders().length) {
            throw new OrderErrorException("order error, please check book id for orders");
        }

        Double sum = list.stream().mapToDouble(Book::getPrice).sum();

        Orders order = new Orders();
        order.setUserId(user.getId());
        order.setSumPrice(sum);
        order.setCreateDate(new Date());
        orderRepository.save(order);

        List<OrderDetail> orderDetailList = new ArrayList<>();
        list.forEach(x -> {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setOrderId(order.getId());
            orderDetail.setBookId(x.getId());
            orderDetailList.add(orderDetail);
        });
        orderDetailRepository.saveAll(orderDetailList);

        return sum;
    }

    private User findUser(Integer userId) {
        Optional<User> optionalUser = userRepository.findById(userId);
        if (optionalUser.isPresent()) {
            return optionalUser.get();
        }

        throw new UserNotFoundException("User not found");
    }
}
