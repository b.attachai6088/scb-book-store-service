package th.co.scb.bookstore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import th.co.scb.bookstore.constant.Constant;
import th.co.scb.bookstore.entity.Book;
import th.co.scb.bookstore.model.BookModelResponse;
import th.co.scb.bookstore.model.BookPublisherModelResponse;
import th.co.scb.bookstore.repository.BookRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private RestTemplate restTemplate;

    public List<BookModelResponse> getBooks() {
        List<Book> bookList = bookRepository.findAllByOrderByIsRecommendedDesc();
        List<BookModelResponse> bookModelResponseList = new ArrayList<>();
        for (Book book : bookList) {
            BookModelResponse bookModelResponse = new BookModelResponse();
            bookModelResponse.setId(book.getId());
            bookModelResponse.setName(book.getName());
            bookModelResponse.setAuthor(book.getAuthor());
            bookModelResponse.setPrice(book.getPrice());
            bookModelResponse.setIs_recommended(book.getIsRecommended());
            bookModelResponseList.add(bookModelResponse);
        }
        return bookModelResponseList;
    }

    public void getBookPublisher() {
        deleteOldBook(); // Clear old book in bookstore

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        List<BookPublisherModelResponse> responseList = restTemplate.exchange(Constant.UrlPublisher.BOOK, HttpMethod.GET, new HttpEntity<>(null, headers), new ParameterizedTypeReference<List<BookPublisherModelResponse>>() {
        }).getBody();
        if (responseList != null && responseList.size() > 0) {
            List<Book> bookList = new ArrayList<>();

            for (BookPublisherModelResponse bookPublisherModelResponse : responseList) {
                Book book = new Book();
                book.setId(bookPublisherModelResponse.getId());
                book.setAuthor(bookPublisherModelResponse.getAuthor_name());
                book.setName(bookPublisherModelResponse.getBook_name());
                book.setPrice(bookPublisherModelResponse.getPrice());
                book.setIsRecommended(false);
                book.setCreateDate(new Date());
                bookList.add(book);
            }

            bookRepository.saveAll(bookList);
        }

        responseList = restTemplate.exchange(Constant.UrlPublisher.BOOK_RECOMMENDATION, HttpMethod.GET, new HttpEntity<>(null, headers), new ParameterizedTypeReference<List<BookPublisherModelResponse>>() {
        }).getBody();
        if (responseList != null && responseList.size() > 0) {
            List<Book> bookList = new ArrayList<>();

            for (BookPublisherModelResponse bookPublisherModelResponse : responseList) {
                Book book = new Book();
                book.setId(bookPublisherModelResponse.getId());
                book.setAuthor(bookPublisherModelResponse.getAuthor_name());
                book.setName(bookPublisherModelResponse.getBook_name());
                book.setPrice(bookPublisherModelResponse.getPrice());
                book.setIsRecommended(true);
                book.setCreateDate(new Date());
                bookList.add(book);
            }

            bookRepository.saveAll(bookList);
        }
    }

    private void deleteOldBook() {
        bookRepository.deleteAll();
    }
}
