package th.co.scb.bookstore;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import th.co.scb.bookstore.component.BookScheduledTasks;

@Slf4j
@EnableScheduling
@SpringBootApplication
public class ScbBookStoreServiceApplication implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(ScbBookStoreServiceApplication.class);

    @Autowired
    private BookScheduledTasks bookScheduledTasks;

    public static void main(String[] args) {
        SpringApplication.run(ScbBookStoreServiceApplication.class, args);
    }

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("Startup application.. run BookScheduledTasks");
        bookScheduledTasks.getBookPublisher();
    }
}
