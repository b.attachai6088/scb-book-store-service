package th.co.scb.bookstore.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class OrderErrorException extends RuntimeException {
    public OrderErrorException(String message) {
        super(message);
    }
}