package th.co.scb.bookstore.exception;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import th.co.scb.bookstore.model.FailureResponse;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        List<String> list = ex.getBindingResult().getFieldErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList());

        FailureResponse<List<String>> response = new FailureResponse<>();
        response.setErrors(list);
        response.setStatus(status.value());

        return handleExceptionInternal(ex, response, headers, status, request);
    }


    @ExceptionHandler(BookNotFoundException.class)
    public ResponseEntity handleBookNotFoundException(BookNotFoundException ex) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        return new ResponseEntity(buildResponse(ex, status), status);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity handleUserNotFoundException(UserNotFoundException ex) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        return new ResponseEntity(buildResponse(ex, status), status);
    }

    @ExceptionHandler(InvalidTokenException.class)
    public ResponseEntity handleInvalidTokenException(InvalidTokenException ex) {
        HttpStatus status = HttpStatus.UNAUTHORIZED;
        return new ResponseEntity(buildResponse(ex, status), status);
    }

    @ExceptionHandler(OrderErrorException.class)
    public ResponseEntity handleOrderErrorException(OrderErrorException ex) {
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        return new ResponseEntity(buildResponse(ex, status), status);
    }

    private FailureResponse<List<String>> buildResponse(Exception ex, HttpStatus status) {
        List<String> list = Arrays.asList(ex.getMessage());
        FailureResponse<List<String>> response = new FailureResponse<>();
        response.setErrors(list);
        response.setStatus(status.value());
        return response;
    }
}
