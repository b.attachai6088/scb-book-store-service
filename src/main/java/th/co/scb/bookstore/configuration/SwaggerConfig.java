package th.co.scb.bookstore.configuration;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import th.co.scb.bookstore.controller.AppController;
import th.co.scb.bookstore.controller.BookController;
import th.co.scb.bookstore.controller.UserController;

import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${application.name}")
    private String applicationName;

    @Value("${build.version}")
    private String buildVersion;

    @Value("${service-config.url.swagger-host}")
    private String host;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .host(host)
                .apiInfo(new ApiInfoBuilder()
                        .version(buildVersion)
                        .title(applicationName)
                        .description("").build())
                .select()
                .apis(basePackages(AppController.class, UserController.class, BookController.class))
                .paths(PathSelectors.any()).build();
    }

    private static Predicate<RequestHandler> basePackages(final Class<?>... classes) {
        final Set<Predicate<RequestHandler>> predicates = new HashSet<>(classes.length);
        for (final Class<?> clazz : classes) {
            final String packageName = clazz.getPackage().getName();
            predicates.add(RequestHandlerSelectors.basePackage(packageName));
        }

        if (predicates.size() == 1) {
            return predicates.iterator().next();
        }

        return Predicates.or(predicates);
    }
}
