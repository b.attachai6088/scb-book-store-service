package th.co.scb.bookstore.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Configuration
@RestController
public class AppController {
    @Value("${application.name}")
    private String applicationName;

    @Value("${build.version}")
    private String buildVersion;

    @Value("${build.timestamp}")
    private String buildTimestamp;

    @ApiOperation(value = "Display Application Informaition")
    @GetMapping(value = "/appinfo")
    public String appInfo() {
        return "ชื่อแอปพลิเคชัน:{0}, เวอร์ชั่น:{1}, วันที่สร้าง:{2}".replace("{0}", applicationName).replace("{1}", buildVersion).replace("{2}", buildTimestamp);
    }
}
