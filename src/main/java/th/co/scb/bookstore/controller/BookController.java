package th.co.scb.bookstore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import th.co.scb.bookstore.model.BookModelResponse;
import th.co.scb.bookstore.service.BookService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/books")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping
    public ResponseEntity<?> getBooks() {
        Map<String, List<BookModelResponse>> response = new HashMap<>();
        response.put("books", bookService.getBooks());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
