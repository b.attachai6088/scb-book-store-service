package th.co.scb.bookstore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import th.co.scb.bookstore.component.AuthenticationFacade;
import th.co.scb.bookstore.model.OrderModelRequest;
import th.co.scb.bookstore.model.UserModelRequest;
import th.co.scb.bookstore.model.UserModelResponse;
import th.co.scb.bookstore.service.UserService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private AuthenticationFacade authenticationFacade;

    @Autowired
    private UserService userService;

    @GetMapping(headers = {"Authorization"})
    public ResponseEntity<UserModelResponse> getUser() {
        return new ResponseEntity<>(userService.getUserDetail(authenticationFacade.getUserLogin().getUserId()), HttpStatus.OK);
    }

    @DeleteMapping(headers = {"Authorization"})
    public void deleteUser() {
        userService.deleteUser(authenticationFacade.getUserLogin().getUserId());
    }

    @PostMapping
    public void postUser(@Valid @RequestBody UserModelRequest userModelRequest) {
        userService.saveUser(userModelRequest);
    }

    @PostMapping(value = "/orders", headers = {"Authorization"})
    public ResponseEntity<?> postOrders(@Valid @RequestBody OrderModelRequest orderModelRequest) {
        Map<String, Double> mapResponse = new HashMap<>();
        mapResponse.put("price", userService.getPrice(orderModelRequest, authenticationFacade.getUserLogin().getUserId()));
        return new ResponseEntity<>(mapResponse, HttpStatus.OK);
    }
}
