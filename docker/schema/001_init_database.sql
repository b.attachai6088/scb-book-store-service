CREATE DATABASE IF NOT EXISTS `bookstoredb` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE `bookstoredb`;

CREATE TABLE IF NOT EXISTS `book` (
    `id`             INT(10) PRIMARY KEY,
    `name`           VARCHAR(200) NOT NULL,
    `author`         VARCHAR(200) NOT NULL,
    `price`          FLOAT        NOT NULL,
    `is_recommended` TINYINT(1)   NOT NULL,
    `create_date`    DATE         NOT NULL
) COMMENT 'book in bookstore' ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `user` (
    `id`            INT(10) AUTO_INCREMENT PRIMARY KEY,
    `username`      VARCHAR(20)  NOT NULL,
    `password`      VARCHAR(500) NOT NULL,
    `name`          VARCHAR(50)  NOT NULL,
    `surname`       VARCHAR(50)  NOT NULL,
    `date_of_birth` DATE         NOT NULL,
    `create_date`   DATE         NOT NULL,
    CONSTRAINT user_username_uindex UNIQUE (`username`)
) COMMENT 'user in bookstore' ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `orders` (
    `id`          INT(10) AUTO_INCREMENT PRIMARY KEY,
    `user_id`     INT(10) NOT NULL,
    `sum_price`   FLOAT   NOT NULL,
    `create_date` DATE    NOT NULL,
    CONSTRAINT order_user_id_fk FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) COMMENT 'order book header' ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `order_detail` (
    `id`       INT(10) AUTO_INCREMENT PRIMARY KEY,
    `order_id` INT(10) NOT NULL,
    `book_id`  INT(10) NOT NULL,
    CONSTRAINT order_detail_order_id_fk FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) COMMENT 'order book detail' ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;